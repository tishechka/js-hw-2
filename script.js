 /*
1) Які існують типи даних у Javascript?

    1. Strings: для текстової інформації.
    2. Numbers.
    3. Boolean: мають два можливих значення - true або false. Використовуються для логічних операцій.
    4. Objects.
    5. Null: показує відсутність значення.
    6. Undefined: показує, що змінна має значення, але його не призначено.
    7. Functions: використовуються для виконання певних дій і повертання значення.


2) У чому різниця між == і ===?

Ці два оператори використовуються для порівняння значень.

Оператор == перевіряє рівність значень з приведенням типів.
Оператор === порівнює значення без приведення типів.


3) Що таке оператор?

Оператор - це символ або ключове слово, яке використовується для виконання різних дій зі змінними.
Він виконує різні види операцій, такі як математичні обчислення, порівняння, логічні операції та інші.

Приклади операторів: +, -, *, /, %, ++, --, =, +=, -=, та інші.
*/

 let name = prompt("What is your name?", "");
 let age = prompt("How old are you?", "");


 while (!name || isNaN(age)) {
     name = prompt("What is your name?", name);
     age = prompt("How old are you?", age);
 }

 if (age < 18) {
     alert("You are not allowed to visit this website.");
 } else if (age >= 18 && age <= 22) {
     const confirmation = confirm("Are you sure you want to continue?");
     if (confirmation) {
         alert("Welcome, " + name + "!");
     } else {
         alert("You are not allowed to visit this website.");
     }
 } else {
     alert("Welcome, " + name + "!");
 }
